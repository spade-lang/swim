## Testing

Run with `cargo test`.

### Downloading or building the Spade compiler

Since Swim is a build tool for Spade, most tests require a Spade compiler. Since
this is a very expensive operation it is disabled by default. The compiler can
take a while to compile, so you can point the test suite to a local copy of the
Spade repository which will be used instead of downloading and building from
scratch.

To use a local copy, use the environment variable `SWIM_LOCAL_SPADE=<path>`, as in

```
SWIM_LOCAL_SPADE=../spade cargo test
```

You can use both relative and absolute paths to refer to the compiler.

To let Swim download and build the compiler, instead set `SWIM_DOWNLOAD_SPADE`:

```
SWIM_DOWNLOAD_SPADE=1 cargo test
```

### Keep temporary directories

When running the tests, a few temporary directories are created and removed
when the tests are done. If you need to look around in these temporary
directories, you can set SWIM_KEEP_TEMP_DIRS to not remove them.
