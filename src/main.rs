/**
 The build tool for the Spade programming language

 For command line parameters, run `swim --help`

 For documentation of the swim.toml format, see [crate::config]

 For documentation of the swim_plugin.toml format, see [crate::plugin::config]
*/
use std::{
    collections::{BTreeMap, HashMap},
    env,
    io::{stdout, IsTerminal},
    process::Command,
};

use camino::{Utf8Path, Utf8PathBuf};
use clap::Parser;
use color_eyre::{
    eyre::{anyhow, bail, Context},
    Result,
};
use colored::Colorize;
use fern::colors::{Color, ColoredLevelConfig};
use itertools::Itertools;
use log::{debug, warn};

use serde::Serialize;
use swim::config::Config;
use swim::init;
use swim::libraries::{restore_libraries, update_libraries, LockFile, RestoreAction};
use swim::links::{handle_url, has_links_installed, setup_links};
use swim::plugin::{load_plugins, restore_plugins, run_command_list};
use swim::pnr::run_pnr;
use swim::simulation::{simulate, SimulationResult, TestFileResult};
use swim::spade::{
    build_spade, build_spade_cxx, build_spadec, restore_spade_repository, update_spade,
};
use swim::synth::synthesise;
use swim::upload::upload;
use swim::util::make_relative;
use swim::{
    build_dir,
    cad_suite_install::{install_oss_cad_suite, maybe_use_oss_cad_suite},
};
use swim::{
    cmdline::{clean, Args, Command as SwimCommand, SimulationArgs},
    spade::generate_docs,
};
use swim::{compiler_state_file, libs_dir, lock_file, logfile_path, test_list_file, CommandCtx};

fn surfer_url(root_dir: &Utf8Path, t: &TestFileResult) -> String {
    let args = vec![
        t.vcd_file.to_string(),
        "--spade-state".to_string(),
        compiler_state_file(root_dir).to_string(),
        "--spade-top".to_string(),
        urlencoding::encode(&t.top_path.join("::")).to_string(),
    ]
    .into_iter()
    .map(|a| format!("arg={}", a))
    .join("&");

    let url = format!(r"swim://surfer?{args}");

    format!("\x1b]8;;{url}\x1b\\[🏄]\x1b]8;;\x1b\\")
}

fn gtkwave_url(t: &TestFileResult) -> String {
    let args = vec![t.vcd_file.to_string()]
        .into_iter()
        .map(|a| format!("arg={}", a))
        .join("&");

    let url = format!(r"swim://gtkwave?{args}");

    format!("\x1b]8;;{url}\x1b\\[🌊]\x1b]8;;\x1b\\")
}

/// This struct is the same as translation::spade::SpadeTestInfo in Surfer
#[derive(Serialize)]
struct SurferInfo {
    state_file: Utf8PathBuf,
    top_names: HashMap<Utf8PathBuf, String>,
}

async fn do_sim(
    root_dir: &Utf8Path,
    args: &Args,
    sim_args: &SimulationArgs,
    ctx: &CommandCtx<'_>,
) -> Result<()> {
    let SimulationResult { result } = simulate(ctx, args, sim_args).await?;
    // Split the results into two Vecs: one with unwrapped Ok values and one with unwrapped Err values.
    // test_results contains the results of running the tests, and test_errors contains errors reported
    // when trying to run the tests.
    let (test_results, test_errors): (Vec<_>, Vec<_>) = result.iter().partition(|r| r.is_ok());
    let test_results = test_results
        .iter()
        .map(|r| r.as_ref().unwrap())
        .collect::<Vec<_>>();
    let test_errors = test_errors
        .iter()
        .map(|r| r.as_ref().unwrap_err())
        .collect::<Vec<_>>();

    // We want to print results per file, not per test case, so we'll have to gather
    // them up accordingly
    let files_with_tests = test_results.iter().fold(
        BTreeMap::new(),
        |mut files: BTreeMap<&Utf8Path, Vec<_>>, r| {
            files.entry(r.test.file()).or_default().push(r);
            files
        },
    );

    let surfer_info = SurferInfo {
        state_file: compiler_state_file(root_dir),
        top_names: files_with_tests
            .iter()
            .flat_map(|(_, test)| {
                test.iter()
                    .map(|test| (make_relative(&test.vcd_file), test.top_path.join("::")))
            })
            .collect(),
    };
    std::fs::write(
        build_dir(root_dir).join("surfer.ron"),
        &ron::to_string(&surfer_info).context("Failed to encode surfer.ron")?,
    )
    .context("Failed to write build/surfer.ron")?;

    let mut num_fails = 0;
    for (file, tests) in files_with_tests.iter().sorted_by_key(|(file, _)| *file) {
        let mut local_fails = 0;
        let cases = tests
            .iter()
            .sorted_by_key(|t| t.test.test_name())
            .map(|result| {
                let status = if result.failure.is_some() {
                    local_fails += 1;
                    "FAILED".red()
                } else {
                    "ok".green()
                };

                format!(
                    " 🭼 {} {} [{}{urls}]",
                    result.test.test_name(),
                    status,
                    make_relative(&result.vcd_file).to_string().dimmed(),
                    urls = if has_links_installed() && stdout().is_terminal() {
                        format!(
                            " ({} {})",
                            surfer_url(root_dir, result),
                            gtkwave_url(result)
                        )
                    } else {
                        "".to_string()
                    }
                )
            })
            .join("\n");

        let status = if local_fails != 0 {
            "FAIL".red()
        } else {
            "ok  ".green()
        };
        println!(
            "\n{status} {file} {local_fails}/{total_tests} failed",
            file = tests
                .first()
                .map(|t| t.relative_tb.clone())
                .unwrap_or(file.to_path_buf()),
            total_tests = tests.len()
        );

        println!("{cases}");

        num_fails += local_fails
    }

    if !test_errors.is_empty() {
        for file in &test_errors {
            println!("{} {}", "FAIL".red(), make_relative(file))
        }
    }

    let mut failure_message_parts = Vec::new();
    if num_fails > 0 {
        failure_message_parts.push(format!(
            "{} test case{} failed",
            num_fails,
            if num_fails == 1 { "" } else { "s" }
        ));
    }
    let num_test_errors = test_errors.len();
    if num_test_errors > 0 {
        failure_message_parts.push(format!(
            "{} error{} in test files",
            num_test_errors,
            if num_test_errors == 1 { "" } else { "s" }
        ))
    }

    match failure_message_parts.len() {
        1 => {
            bail!("{}", failure_message_parts[0]);
        }
        2.. => {
            bail!(
                "{} and {}",
                &failure_message_parts[..failure_message_parts.len() - 1].join(", "),
                failure_message_parts.last().unwrap(),
            );
        }
        _ => (),
    }

    // Write a list of all the test cases
    let test_list_file = test_list_file(root_dir);
    std::fs::write(
        &test_list_file,
        serde_json::to_string_pretty(
            &test_results
                .iter()
                .map(|r| root_dir.join(&r.vcd_file))
                .collect::<Vec<_>>(),
        )
        .context("Failed to json encode test list")?,
    )
    .with_context(|| format!("Failed to write test list to {test_list_file}"))?;

    Ok(())
}

async fn prepare_normal_flow<'a>(root_dir: &'a Utf8Path, args: &'a Args) -> Result<CommandCtx<'a>> {
    let config = Config::read(root_dir, &args.override_compiler)?;

    let compiler = build_spadec(root_dir, args, &config).await?;
    let plugins = load_plugins(root_dir, &config, RestoreAction::Deny)?;
    if plugins.inner.iter().any(|(_, p)| p.requires_cxx) {
        build_spade_cxx(root_dir, args, &config).await?;
    }
    plugins.run_preprocessing_commands()?;
    Ok(CommandCtx {
        args,
        config,
        compiler,
        plugins,
        root_dir,
    })
}

async fn builtin_command(root_dir: &Utf8Path, args: Args) -> Result<()> {
    debug!("Built-in command {args:?}");

    match &args.command {
        SwimCommand::Build => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            build_spade(&ctx)?;
        }
        SwimCommand::Doc => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            generate_docs(&ctx, false)?;
        }
        SwimCommand::Synth { yosys_command_file } => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            synthesise(&ctx, yosys_command_file)?;
        }
        SwimCommand::Pnr { gui } => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            run_pnr(&ctx, *gui)?;
        }
        SwimCommand::Upload { use_sudo } => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            upload(&ctx, *use_sudo)?;
        }
        SwimCommand::Simulate(sim_args) => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;
            do_sim(root_dir, &args, sim_args, &ctx).await?;
        }
        SwimCommand::Plugin {
            name,
            args: plugin_args,
        } => {
            let ctx = prepare_normal_flow(root_dir, &args).await?;

            // Figure out which command to run
            let relevant_commands = ctx.plugins.commands_matching(name);

            let (plugin_info, cmd) = match relevant_commands.as_slice() {
                [] => bail!("No plugin defines a command {name}"),
                [single] => single,
                many => {
                    bail!(
                        "Several plugins define {name}. It is defined in [{}]",
                        many.iter().map(|cmd| &cmd.0 .0).join(", ")
                    )
                }
            };

            // Run command prerequisites
            match cmd.after {
                swim::plugin::config::BuildStep::Start => {}
                swim::plugin::config::BuildStep::SpadeBuild => {
                    build_spade(&ctx)?;
                }
                swim::plugin::config::BuildStep::Simulation => {
                    do_sim(root_dir, &args, &Default::default(), &ctx).await?;
                }
                swim::plugin::config::BuildStep::Synthesis => {
                    synthesise(&ctx, &None)?;
                }
                swim::plugin::config::BuildStep::Pnr => {
                    run_pnr(&ctx, false)?;
                }
                swim::plugin::config::BuildStep::Upload => {
                    upload(&ctx, false)?;
                }
            }

            // Unescape shell strings
            let arg_list = plugin_args.iter().map(|a| format!("'{}'", a)).join(" ");
            run_command_list(
                &plugin_info.0,
                &cmd.script
                    .iter()
                    .map(|s| s.replace("%args%", &arg_list))
                    .collect::<Vec<_>>(),
                "user command",
            )?;
        }
        SwimCommand::Init {
            dir,
            template_repo,
            board,
            list_boards,
            skip_checking_files,
        } => {
            if *list_boards {
                init::list_boards(template_repo)?;
            } else {
                let dir = match dir {
                    Some(dir) => dir.to_owned(),
                    None => std::env::current_dir()?.try_into()?,
                };
                init::init(&dir, template_repo, board.as_deref(), *skip_checking_files)?;
            }
        }
        SwimCommand::Update => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            update_libraries(&libs_dir(root_dir), &config, &mut lock_file)?;
            if let Some(libraries) = &mut lock_file.inner.libraries {
                libraries.filter_unseen();
            }
            lock_file.try_write()?;
        }
        SwimCommand::UpdateSpade => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            update_spade(root_dir, &config)?;
        }
        SwimCommand::Restore => {
            let config = Config::read(root_dir, &args.override_compiler)?;
            let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
            restore_libraries(root_dir, &config, &mut lock_file)?;
            restore_plugins(root_dir, &config, &mut lock_file)?;
            restore_spade_repository(root_dir, &config)?;
            if let Some(libraries) = &mut lock_file.inner.libraries {
                libraries.filter_unseen();
            }
            lock_file.try_write()?;
        }
        SwimCommand::Clean { and_spade } => {
            clean(root_dir, *and_spade)?;
        }
        SwimCommand::SetupLinks => {
            setup_links()?;
        }
        SwimCommand::Url { url } => {
            handle_url(url)?;
        }
        SwimCommand::InstallTools { reinstall } => install_oss_cad_suite(*reinstall).await?,
    }

    Ok(())
}

/// Try to parse and run an external command with `swim-`-prefix.
///
/// For example, calling `swim lifeguard` will try to execute `swim-lifeguard`
/// with the rest of the arguments passed.
///
/// The outer [Result] represents if the command was run or not. The inner
/// [Result] represents whether the command exited with a zero exit status or not.
fn external_command() -> Result<Result<()>> {
    let mut args = std::env::args();
    // When calling a custom command, the full argument list will be
    // `swim command arg1, arg2...`
    // The `swim` part is irrelevant here
    let _ = args.next(); // `swim` or whatever our binary is named.
                         // The sub-command to be called is the first command
    let command = args.next().ok_or(anyhow!("no command"))?;
    // Because we used `next` to pop commands of the list, the rest of `args` now contains the remaining arguments which should be passed to the subcommanad

    let status = Command::new(format!("swim-{}", command))
        .args(args)
        .status()?;

    if status.success() {
        Ok(Ok(()))
    } else {
        Ok(Err(anyhow!("non-zero exit status")))
    }
}

fn setup_logging<'p>(logfile: impl Into<Option<&'p Utf8Path>>) -> Result<()> {
    let colors = ColoredLevelConfig::new()
        .error(Color::Red)
        .warn(Color::Yellow)
        .info(Color::Green)
        .debug(Color::Blue)
        .trace(Color::White);

    let file_config = if let Some(logfile) = logfile.into() {
        std::fs::create_dir_all(
            logfile
                .parent()
                .expect("swim.log path has no parent directory"),
        )?;
        Some(
            fern::Dispatch::new()
                .level(log::LevelFilter::Debug)
                .format(move |out, message, record| {
                    out.finish(format_args!(
                        "{}[{}] {}",
                        chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                        record.level(),
                        message
                    ))
                })
                .chain(fern::log_file(logfile)?),
        )
    } else {
        None
    };

    let max_level = env::var("SWIM_LOG")
        .map(|v| match v.as_str() {
            "trace" => Ok(log::LevelFilter::Trace),
            "debug" => Ok(log::LevelFilter::Debug),
            "info" => Ok(log::LevelFilter::Info),
            "warn" => Ok(log::LevelFilter::Warn),
            "error" => Ok(log::LevelFilter::Error),
            "off" => Ok(log::LevelFilter::Off),
            other => bail!("Unsupported log level {other}"),
        })
        .unwrap_or(Ok(log::LevelFilter::Info))?;
    let stdout_config = fern::Dispatch::new()
        .level(max_level)
        .format(move |out, message, record| {
            out.finish(format_args!(
                "[{}] {}",
                colors.color(record.level()),
                message
            ))
        })
        .chain(std::io::stdout());

    let dispatch = fern::Dispatch::new().chain(stdout_config);

    let dispatch = if let Some(file_config) = file_config {
        dispatch.chain(file_config)
    } else {
        dispatch
    };
    dispatch.apply()?;

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    let current_dir = Utf8PathBuf::try_from(std::env::current_dir()?)?;
    let args = swim::cmdline::Args::try_parse();

    // We don't want to create a swim.log if the user hasn't "opted into swim". We assume that a
    // valid swim.toml means we can go into a build directory and put the log there.

    // Try to read a config without caring about the actual result.
    // NOTE: Let's hope that parsing the config is cheap.
    let has_valid_toml = Config::read(&current_dir, &None).is_ok();

    let logfile = if has_valid_toml {
        Some(logfile_path(&current_dir))
    } else {
        None
    };

    setup_logging(logfile.as_deref())?;

    match args {
        Ok(args) => {
            if let Err(e) = maybe_use_oss_cad_suite(&args) {
                warn!("Failed to check if cad tools are installed by swim. Falling back to system executables\n{e:#?}")
            };

            builtin_command(&current_dir, args).await
        }
        Err(e) => {
            // If an external command exists, try to use it.
            match external_command() {
                Ok(inner) => inner,
                // No external command was found so report the original clap error instead.
                Err(_) => e.exit(),
            }
        }
    }
}
