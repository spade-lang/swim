use serde::Deserialize;

pub trait NextpnrExtraArgs {
    fn pin_file_args(&self, pin_file: &str) -> Vec<String>;
    fn device_args(&self) -> Vec<String>;
}

#[derive(Deserialize, Clone, Debug)]
pub enum Ice40Device {
    #[serde(rename = "iCE40LP384")]
    Ice40lp384,
    #[serde(rename = "iCE40LP1K")]
    Ice40lp1k,
    #[serde(rename = "iCE40LP4K")]
    Ice40lp4k,
    #[serde(rename = "iCE40LP8K")]
    Ice40lp8k,
    #[serde(rename = "iCE40HX1K")]
    Ice40hx1k,
    #[serde(rename = "iCE40HX4K")]
    Ice40hx4k,
    #[serde(rename = "iCE40HX8K")]
    Ice40hx8k,
    #[serde(rename = "iCE40UP3K")]
    Ice40up3k,
    #[serde(rename = "iCE40UP5K")]
    Ice40up5k,
    #[serde(rename = "iCE5LP1K")]
    Ice5lp1k,
    #[serde(rename = "iCE5LP2K")]
    Ice5lp2k,
    #[serde(rename = "iCE5LP4K")]
    Ice5lp4k,
}

#[derive(Deserialize, Clone, Debug)]
pub struct Ice40Args {
    pub package: String,
    pub device: Ice40Device,
}

impl NextpnrExtraArgs for Ice40Args {
    fn device_args(&self) -> Vec<String> {
        let package_flag = match self.device {
            Ice40Device::Ice40lp384 => "--lp384",
            Ice40Device::Ice40lp1k => "--lp1k",
            Ice40Device::Ice40lp4k => "--lp4k",
            Ice40Device::Ice40lp8k => "--lp8k",
            Ice40Device::Ice40hx1k => "--hx1k",
            Ice40Device::Ice40hx4k => "--hx4k",
            Ice40Device::Ice40hx8k => "--hx8k",
            Ice40Device::Ice40up3k => "--up3k",
            Ice40Device::Ice40up5k => "--up5k",
            Ice40Device::Ice5lp1k => "--u1k",
            Ice40Device::Ice5lp2k => "--u2k",
            Ice40Device::Ice5lp4k => "--u4k",
        };
        vec![
            package_flag.to_string(),
            "--package".to_string(),
            self.package.clone(),
        ]
    }

    fn pin_file_args(&self, pin_file: &str) -> Vec<String> {
        vec!["--pcf".to_string(), pin_file.to_string()]
    }
}

#[derive(Deserialize, Clone, Debug)]
pub struct Ecp5Args {
    pub package: String,
    pub device: Ecp5Device,
}

impl NextpnrExtraArgs for Ecp5Args {
    fn device_args(&self) -> Vec<String> {
        let package_flag = match self.device {
            Ecp5Device::Lfe5u12f => "--12k",
            Ecp5Device::Lfe5u25f => "--25k",
            Ecp5Device::Lfe5u45f => "--45k",
            Ecp5Device::Lfe5u85f => "--85k",
            Ecp5Device::Lfe5um25f => "--um-25k",
            Ecp5Device::Lfe5um45f => "--um-45k",
            Ecp5Device::Lfe5um85f => "--um-85k",
            Ecp5Device::Lfe5um5g25f => "--um5g-25k",
            Ecp5Device::Lfe5um5g45f => "--um5g-45k",
            Ecp5Device::Lfe5um5g85f => "--um5g-85k",
        };
        vec![
            package_flag.to_string(),
            "--package".to_string(),
            self.package.clone(),
        ]
    }

    fn pin_file_args(&self, pin_file: &str) -> Vec<String> {
        vec!["--lpf".to_string(), pin_file.to_string()]
    }
}

#[derive(Deserialize, Clone, Debug)]
pub enum Ecp5Device {
    #[serde(rename = "LFE5U-12F")]
    Lfe5u12f,
    #[serde(rename = "LFE5U-25F")]
    Lfe5u25f,
    #[serde(rename = "LFE5U-45F")]
    Lfe5u45f,
    #[serde(rename = "LFE5U-85F")]
    Lfe5u85f,
    #[serde(rename = "LFE5UM-25F")]
    Lfe5um25f,
    #[serde(rename = "LFE5UM-45F")]
    Lfe5um45f,
    #[serde(rename = "LFE5UM-85F")]
    Lfe5um85f,
    #[serde(rename = "LFE5UM5G-25F")]
    Lfe5um5g25f,
    #[serde(rename = "LFE5UM5G-45F")]
    Lfe5um5g45f,
    #[serde(rename = "LFE5UM5G-85F")]
    Lfe5um5g85f,
}

#[derive(Deserialize, Clone, Debug)]
pub enum Lv9QN88PC6Family {
    #[serde(rename = "GW1N-9C")]
    Gw1n9c,
    /// Specify a raw string for the family instead of the swim-provided families. Used
    /// if swim doesn't support the configuration you want.
    String(String),
}
impl Lv9QN88PC6Family {
    fn string(&self) -> String {
        match self {
            Self::Gw1n9c => "GW1N-9C".to_string(),
            Self::String(s) => s.to_string(),
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
pub enum Lv1qn48c6I5Family {
    #[serde(rename = "GW1NZ-1")]
    Gw1nz1,
    /// Specify a raw string for the family instead of the swim-provided families. Used
    /// if swim doesn't support the configuration you want.
    String(String),
}
impl Lv1qn48c6I5Family {
    fn string(&self) -> String {
        match self {
            Self::Gw1nz1 => "GW1NZ-1".to_string(),
            Self::String(s) => s.to_string(),
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
pub enum Lv18QNFamily {
    #[serde(rename = "GW2A-18C")]
    Gw2a18c,
    #[serde(rename = "GW2AR-18C")]
    Gw2ar18c,
    #[serde(rename = "GW2ANR-18C")]
    Gw2anr18c,
    /// Specify a raw string for the family instead of the swim-provided families. Used
    /// if swim doesn't support the configuration you want.
    String(String),
}
impl Lv18QNFamily {
    fn string(&self) -> String {
        match self {
            Self::Gw2a18c => "GW2A-18C".to_string(),
            Self::Gw2ar18c => "GW2AR-18C".to_string(),
            Self::Gw2anr18c => "GW2ANR-18C".to_string(),
            Self::String(s) => s.to_string(),
        }
    }
}

#[derive(Deserialize, Clone, Debug)]
#[allow(non_camel_case_types)]
pub enum GowinDevice {
    #[serde(rename = "GW1NR-UV9QN881C6/I5")]
    GW1NR_UV9QN881C6_I5,
    #[serde(rename = "GW1N-LV1QN48C6/I5")]
    GW1N_LV1QN48C6_I5 { family: Lv1qn48c6I5Family },
    #[serde(rename = "GW1NZ-LV1QN48C6/I5")]
    GW1NZ_LV1QN48C6_I5,
    #[serde(rename = "GW1NSR-LV4CQN48PC7/I6")]
    GW1NSR_LV4CQN48PC7_I6,
    #[serde(rename = "GW1NR-LV9QN88PC6/I5")]
    GW1NR_LV9QN88PC6_I5 { family: Lv9QN88PC6Family },
    #[serde(rename = "GW2AR-LV18QN88C8/I7")]
    GW2AR_LV18QN88C8_I7 { family: Lv18QNFamily },
    #[serde(rename = "GW2A-LV18PG256C8/I7")]
    GW2A_LV18PG256C8_I7 { family: Lv18QNFamily },
    #[serde(rename = "GW1N-UV4LQ144C6/I5")]
    GW1N_UV4LQ144C6_I5,
    #[serde(rename = "GW1NS-UX2CQN48C5/I4")]
    GW1NS_UX2CQN48C5_I4,
    #[serde(rename = "GW1NR-LV9LQ144PC6/I5")]
    GW1NR_LV9LQ144PC6_I5,
}

#[derive(Deserialize, Clone, Debug)]
pub struct GowinArgs {
    pub device: GowinDevice,
}

impl NextpnrExtraArgs for GowinArgs {
    fn device_args(&self) -> Vec<String> {
        let (device, family) = match &self.device {
            GowinDevice::GW1NR_UV9QN881C6_I5 => ("GW1NR-UV9QN881C6/I5", None),
            GowinDevice::GW1N_LV1QN48C6_I5 { family } => {
                ("GW1N-LV1QN48C6/I5", Some(family.string()))
            }
            GowinDevice::GW1NZ_LV1QN48C6_I5 => ("GW1NZ-LV1QN48C6/I5", None),
            GowinDevice::GW1NSR_LV4CQN48PC7_I6 => ("GW1NSR-LV4CQN48PC7/I6", None),
            GowinDevice::GW1NR_LV9QN88PC6_I5 { family } => {
                ("GW1NR-LV9QN88PC6/I5", Some(family.string()))
            }
            GowinDevice::GW2AR_LV18QN88C8_I7 { family } => {
                ("GW2AR-LV18QN88C8/I7", Some(family.string()))
            }
            GowinDevice::GW2A_LV18PG256C8_I7 { family } => {
                ("GW2A-LV18PG256C8/I7", Some(family.string()))
            }
            GowinDevice::GW1N_UV4LQ144C6_I5 => ("GW1N-UV4LQ144C6/I5", None),
            GowinDevice::GW1NS_UX2CQN48C5_I4 => ("GW1NS-UX2CQN48C5/I4", None),
            GowinDevice::GW1NR_LV9LQ144PC6_I5 => ("GW1NR-LV9LQ144PC6/I5", None),
        };
        let mut result = vec!["--device".to_string(), device.to_string()];
        if let Some(family) = family {
            result.append(&mut vec!["--vopt".to_string(), format!("family={family}")]);
        }
        result
    }

    fn pin_file_args(&self, pin_file: &str) -> Vec<String> {
        vec!["--vopt".to_string(), format!("cst={}", pin_file)]
    }
}
