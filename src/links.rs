use std::{path::PathBuf, process::Command};

use color_eyre::{
    eyre::{anyhow, bail, Context},
    Result,
};
use indoc::formatdoc;
use log::info;
use url::Url;

use crate::util::make_relative;

const SCHEME_HANDLER: &str = "swim";
const DESKTOP_FILE_NAME: &str = "swim_scheme_handler.desktop";

fn xdg_desktop_file() -> Result<PathBuf> {
    Ok(xdg::BaseDirectories::new()
        .context("Failed to look up the xdg base directories")?
        .get_data_home()
        .join("applications")
        .join(DESKTOP_FILE_NAME))
}

fn setup_desktop_file() -> Result<()> {
    let executable = std::env::current_exe()
        .context("Failed to find the current swim executable")?
        .to_str()
        .ok_or_else(|| anyhow!("The current executable path was not unicode"))?
        .to_string();

    let desktop_file = formatdoc! {
        "
        [Desktop Entry]
        Type=Application
        Name=DDG Scheme Handler
        Exec={executable} url %u
        StartupNotify=false
        MimeType=x-scheme-handler/{SCHEME_HANDLER};
        "
    };

    let desktop_file_location = xdg_desktop_file()?;

    if desktop_file_location.exists() {
        bail!("{} already exists", desktop_file_location.to_string_lossy());
    }

    // This print is relative because we want snapshot testing to not care about where the
    // tests are run, and the snapshot tests run from the build directory. In practice, when
    // calling `swim setup-links` this will still be an absolute path
    info!(
        "Writing desktop file to associate swim:// with {}",
        make_relative(&executable)
    );
    std::fs::write(&desktop_file_location, &desktop_file).with_context(|| {
        format!(
            "Failed to write {}",
            desktop_file_location.to_string_lossy()
        )
    })
}

fn associate_mime_type() -> Result<()> {
    info!("Running xdg-mime to associate swim:// with swim");
    let status = Command::new("xdg-mime")
        .arg("default")
        .arg(DESKTOP_FILE_NAME)
        .arg(format!("x-scheme-handler/{SCHEME_HANDLER}"))
        .status()
        .context("Failed to run xdg-mime")?;

    if !status.success() {
        bail!("Setting the x-scheme-handler with xdg-mime failed")
    }
    Ok(())
}

pub fn setup_links() -> Result<()> {
    setup_desktop_file()?;
    associate_mime_type()?;

    Ok(())
}

pub fn handle_url(url: &str) -> Result<()> {
    let parsed = Url::parse(url).context("Failed to parse url")?;

    let args = parsed
        .query_pairs()
        .into_iter()
        .filter_map(|(l, r)| {
            if l == "arg" {
                Some(r.to_string())
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    match parsed.host() {
        Some(url::Host::Domain("surfer")) => {
            Command::new("surfer")
                .args(args)
                .spawn()
                .with_context(|| "Failed to run surfer")?;
        }
        Some(url::Host::Domain("gtkwave")) => {
            Command::new("gtkwave")
                .args(args)
                .spawn()
                .with_context(|| "Failed to run surfer")?;
        }
        Some(other) => {
            bail!("Unknown 'host' {other}")
        }
        None => {
            bail!("No host")
        }
    }

    Ok(())
}

/// Checks if swim:// links are installed. If this fails, it will fail silently to
/// avoid problems if, for example, the user does not have xdg-mime set
pub fn has_links_installed() -> bool {
    xdg_desktop_file().map(|f| f.exists()).unwrap_or(false)
}
