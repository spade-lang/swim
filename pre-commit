#!/bin/bash

SOURCE_DIRS=('*/src/*')

# Some runt tests have swim run git diff which fails in pre-commit hoooks
# unless we unset GIT_INDEX_FILE
# We also want to pre-warm runt in order to not get diffs since the output
# may differ between the first and second run
function run_runt {(
    unset GIT_INDEX_FILE && \
    cargo build && \
    cd runt && \
    (runt || true) && \
    runt -d
)}


function check_untracked_srcs {(
    untracked=$(git ls-files --others --exclude-standard -- ${SOURCE_DIRS[@]})
    if [[ "$untracked" != "" ]]; then
        echo -e "[\033[0;43mwarn\033[0m]Untracked files:\n\033[0;33m$untracked\033[0m"
    fi
)}

# Set GIT_INDEX_FILE to something else since otherwise, clones in the test
# suite makes us unable to `git commit <file>`.
# 
# See https://gitlab.com/spade-lang/swim/-/issues/34.
indexfile=$(mktemp)

# Parens are there to start a new shell where we can exit early on failure
function run_tests {(
    cargo +stable fmt -- --check && \
    GIT_INDEX_FILE="$indexfile" cargo test -- --test-threads=1 && \
    true
    # run_runt && \
)}

success=0
if run_tests ; then
    success=1
else
    echo "----------------------------------------------------"
    echo -e "[\033[0;41merror\033[0m] Local tests failed. Force a commit using --no-verify"
fi

rm -f $indexfile

if [[ $success -eq 0 ]]; then
    exit 1
fi

check_untracked_srcs
