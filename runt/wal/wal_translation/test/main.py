#top = main::main

from cocotb import *
from cocotb.triggers import Timer

@cocotb.test()
async def run(dut):
    await Timer(1, units="ns")
